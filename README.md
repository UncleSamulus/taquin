# Taquin

Le jeu du taquin, une application web, un script python implantant l'algorithme A* et les sources .TeX du rapport et du diapo de soutenance du projet.

***

Ce projet fait partie de l'UE *Intelligence Artificielle* en L3 Informatique à l'Université d'Évry - val d'Essonne.

<!-- ## Visuals -->

## Dates

- Début du projet: 10 Février 2023
- Date de rendu: 5 Avril 2023

## Auteurs

- Naïa PÉRINELLE
- Samuel ORTION
