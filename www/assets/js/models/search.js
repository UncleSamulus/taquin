import {
	MinPriorityQueue,
  } from '@datastructures-js/priority-queue';

function aStar(initialNode, isGoal, expandNode) {
	let frontier = new MinPriorityQueue((node) => node.evaluation);

	frontier.enqueue(initialNode);
	let explored = new Set();
	while (! frontier.isEmpty()) {
		let currentNode = frontier.dequeue();
		if (isGoal(currentNode.value)) {
			return currentNode;
		} else {
			explored.add(currentNode.value);
			let children = expandNode(currentNode);
			for (let child of children) {
				if (! explored.has(child.value)) {
					frontier.enqueue(child);
				}
			}
		}
	} 
	return null;
}

export {
	aStar
}