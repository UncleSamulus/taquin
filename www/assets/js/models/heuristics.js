const HEURISTICS = {
	"h1": {
		"pi": [36, 12, 12, 4, 1, 1, 4, 1, 0],
		"rho": 4
	},
	"h2": {
		"pi": [8, 7, 6, 5, 4, 3, 2, 1, 0],
		"rho": 1
	},
	"h3": {
		"pi": [8, 7, 6, 5, 4, 3, 2, 1, 0],
		"rho": 4
	},
	"h4": {
		"pi": [8, 7, 6, 5, 3, 2, 4, 1, 0],
		"rho": 1
	},
	"h5": {
		"pi": [8, 7, 6, 5, 3, 2, 4, 1, 0],
		"rho": 4
	},
	"h6": {
		"pi": [1, 1, 1, 1, 1, 1, 1, 1, 0],
		"rho": 1
	}
}

function heuristics(names, state, size) {
	return names.reduce((acc, name) => {
		let value = heuristic(name, state, size);
		if (value > acc) 
			return value;
		else
			return acc;
	}, 0);
}

function heuristic(name, state, size){
	let pi = HEURISTICS[name]["pi"]
	let rho = HEURISTICS[name]["rho"]
	let sum = state.reduce((acc, value, index) => {
		return acc + pi[value] * manhattan(state, size, index);
	})
	return Math.floor(sum / rho);
}

function manhattan(state, size, index) {
	let x = Math.floor(index / size);
	let y = index % size;
	let value = state[index];
	let targetX = Math.floor(value / size);
	let targetY = value % size;
	return Math.abs(x - targetX) + Math.abs(y - targetY);
}

export {
	heuristic,
	heuristics
}