const DIRECTION_INCREMENTS = [
	[0, 1],
	[0, -1],
	[-1, 0],
	[1, 0]
];

function initPuzzle(boardSize) {
	let stateArray = Array.from(Array(boardSize * boardSize).keys());
	let shuffleIterations = 1000;
	return shufflePuzzle(boardSize, stateArray, shuffleIterations);
}

function performMove(boardSize, stateArray, directionIncrement) {
	let newStateArray = stateArray.slice();
	let emptyValue = boardSize * boardSize - 1;
	let emptyPosition = stateArray.indexOf(emptyValue);
	let emptyRow = Math.floor(emptyPosition / boardSize);
	let emptyCol = emptyPosition % boardSize;
	let cellRow = emptyRow - directionIncrement[0];
	let cellCol = emptyCol - directionIncrement[1];
	if (cellRow < 0 || cellRow >= boardSize || cellCol < 0 || cellCol >= boardSize) {
		return null;
	}
	let cellPosition = cellRow * boardSize + cellCol;
	let cellValue = newStateArray[cellPosition];
	newStateArray[cellPosition] = emptyValue;
	newStateArray[emptyPosition] = cellValue;
	emptyPosition = cellPosition;
	emptyRow = cellRow;
	emptyCol = cellCol;
	return newStateArray;
}

function shufflePuzzle(boardSize, stateArray, shuffleIterations) {
	for (let i = 0; i < shuffleIterations; i++) {
		let directionIncrement = DIRECTION_INCREMENTS[Math.floor(Math.random() * 4)];
		let newStateArray = performMove(boardSize, stateArray, directionIncrement);
		if (newStateArray == null || newStateArray == stateArray) {
			continue;
		}
		stateArray = newStateArray;
	}
	return stateArray;
}

function manhattanDistance(boardSize, stateArray, goalArray) {
	let distance = 0;
	for (let i = 0; i < stateArray.length; i++) {
		let value = stateArray[i];
		let goalPosition = goalArray.indexOf(value);
		let goalRow = Math.floor(goalPosition / boardSize);
		let goalCol = goalPosition % boardSize;
		let statePosition = i;
		let stateRow = Math.floor(statePosition / boardSize);
		let stateCol = statePosition % boardSize;
		distance += Math.abs(goalRow - stateRow) + Math.abs(goalCol - stateCol);
	}
	return distance;
}

function isGoal(stateArray) {
	for (let i = 0; i < stateArray.length; i++) {
		if (stateArray[i] != i) {
			return false;
		}
	}
	return true;
}

function generateSuccessors(currentState, boardSize) {
	let successors = [];

	for (let i = 0; i < DIRECTION_INCREMENTS.length; i++) {
		let directionIncrement = DIRECTION_INCREMENTS[i];
		let successor = currentState.slice();
		successor = performMove(boardSize, successor, directionIncrement);
		if (successor === null || successor == currentState) {
			continue;
		}
		successors.push(successor);
	}
	return successors;
}

function reconstructAction(currentState, cameFrom, boardSize) {
	let predecessor = cameFrom;
	let predecessorPosition = predecessor.indexOf(boardSize * boardSize - 1);
	let predecessorRow = Math.floor(predecessorPosition / boardSize);
	let predecessorCol = predecessorPosition % boardSize;
	let statePosition = currentState.indexOf(boardSize * boardSize - 1);
	let stateRow = Math.floor(statePosition / boardSize);
	let stateCol = statePosition % boardSize;
	let directionIncrement = [predecessorRow - stateRow, predecessorCol - stateCol];
	return directionIncrement;
}

export {
	initPuzzle,
	performMove,
	manhattanDistance,
	isGoal,
	generateSuccessors,
	reconstructAction
}