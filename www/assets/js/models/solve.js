import { aStar } from "./search"
import Node from "./Node"
import { generateSuccessors, isGoal } from "./taquin"
import { heuristics } from "./heuristics";
import Config from "./config";

function expandNode(node, boardSize) {
	let children = [];
	let successors = generateSuccessors(node.value, boardSize);
	for (let successor of successors) {
		let child = new Node(successor);
		child.parent = node;
		child.cost = node.cost + 1;
		child.heuristic = heuristics(Config.heuristics, child.value, boardSize);
		child.evaluation = child.cost + child.heuristic;
		children.push(child);
	}
	return children;
}

function expandNodeFunctor(boardSize) {
	return (node) => expandNode(node, boardSize);
}

function solve(initialState, boardSize) {
	let initialNode = new Node(initialState);
	let leafNode = aStar(initialNode, isGoal, expandNodeFunctor(boardSize));
	if (leafNode == null) {
		return null;
	} else {
		return pathRootToLeaf(leafNode);
	}
}

function pathRootToLeaf(node) {
	let path = [];
	while (node != null) {
		path.push(node.value);
		node = node.parent;
	}
	return path.reverse();
}

export { solve };