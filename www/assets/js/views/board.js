import { reconstructAction } from "../models/taquin";

const View = {
	moveCounter: document.querySelector(".moves.value"),
	manhattanCounter: document.querySelector(".manhattan.value"),
	heuristicCounter: document.querySelector(".heuristic.value"),
	board: document.querySelector(".board.game"),
	exampleBoard: document.querySelector(".board.goal"),
	history: document.querySelector(".history.game"),
	solution: document.querySelector(".solution.history"),
	solutionSteps: document.querySelector(".solution.steps"),
	newGameButton: document.querySelector(".button.new-game"),
	resetButton: document.querySelector(".button.reset"),
	undoButton: document.querySelector(".button.undo"),
	helpButton: document.querySelector(".button.help"),
	solveButton: document.querySelector(".button.solve"),
	menuStartButton: document.querySelector('.button.menu-start'),
	menuHeuristicSelectButton: document.querySelector('.button.select-heuristics'),
	menuHeuristicCheckboxes: document.getElementsByName('heuristic'),
	openMenuButton: document.querySelector(".button.open-menu"),
	closeMenuButton: document.querySelector(".button.close-menu"),
	openHelpButton: document.querySelector(".button.open-help"),
	closeHelpButton: document.querySelector(".button.close-help"),
	menu: document.querySelector("#menu"),
	help: document.querySelector("#help"),	

	createBoard,
	fillBoard,
	update: updateView,
	fillHistory,
	clearHistory,
	clearSolution
}

/**
 * Create a boardSize * boardSize div based table
 * 
 * @param {HTMLDivElement} htmlParentElement 
 * @param {Number} boardSize 
 */
function createBoard(htmlParentElement, boardSize) {
	htmlParentElement.style = `--n: ${boardSize};`
	Array(boardSize * boardSize).fill(0).forEach((_, index) => {
		let cellElement = document.createElement("div");
		cellElement.classList.add("cell");
		cellElement.setAttribute("index", index);
		htmlParentElement.appendChild(cellElement);
	});
}


/**
 * Fill the html board with the given state array
 * 
 * @param {HTMLDivElement} htmlParentElement 
 * @param {Number} boardSize 
 * @param {Array} stateArray 
 */
function fillBoard(htmlParentElement, boardSize, stateArray) {
	for (let i = 0; i < boardSize * boardSize; i++) {
		let cellElement = htmlParentElement.children[i];
		let value = stateArray[i];
		if (value === boardSize * boardSize - 1) {
			cellElement.classList.add('empty');
			cellElement.innerText = "";
		} else {
			cellElement.classList.remove("empty")
			cellElement.innerText = value;
		}
	}
}

function updateView(game) {
	fillBoard(View.board, game.size, game.current);
	View.manhattanCounter.innerText = game.distance;
	View.heuristicCounter.innerText = game.heuristic;	
	View.moveCounter.innerText = game.moves;
}

const DIRECTIONS = {
	"gauche": [0, -1],
	"droite": [0, 1],
	"haut": [-1, 0],
	"bas": [1, 0]
}

function direction(directionIncrement) {
	for (let name in DIRECTIONS) {
		if (DIRECTIONS[name][0] === directionIncrement[0] && DIRECTIONS[name][1] === directionIncrement[1]) {
			return name;
		}
	}
}

function fillHistory(historyElement, stateHistory, boardSize) {
	clearHistory(historyElement);
	stateHistory.forEach((state, index) => {
		let previousState = stateHistory[index - 1];
		let action, directionIncrement;
		if (previousState === undefined) {
			action = "État initial";
		} else {
			directionIncrement = reconstructAction(previousState, state, boardSize);
			action = direction(directionIncrement)
		}
		let historyItem = document.createElement("div");
		historyItem.classList.add("state");
		let actionElement = document.createElement("div");
		actionElement.classList.add("action");
		actionElement.innerText = action;
		historyItem.appendChild(actionElement);
		let boardElement = document.createElement("div");
		boardElement.classList.add("board");
		createBoard(boardElement, boardSize);
		fillBoard(boardElement, boardSize, state);
		historyItem.appendChild(boardElement);
		historyElement.appendChild(historyItem);
	});
}

function clearHistory() {
	View.history.innerHTML = "";
}

function clearSolution() {
	View.solution.innerHTML = "";
	View.solutionSteps.innerText = "\(x\)";
}

export default View;