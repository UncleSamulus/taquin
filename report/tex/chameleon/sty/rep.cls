% This template aims to simplify and improve the (Xe)LaTeX template
% provided by the EPFL. Original template by EPFL. Rewritten
% template by Daan Zwaneveld (https://dzwaneveld.github.io). EPFL
% adapted template by Batuhan Faik Derinbay.
% (https://github.com/batuhanfaik/EPFL-Report-Template)
%
% This template is available under CC BY-NC 4.0. For more information,
% see https://creativecommons.org/licenses/by-nc/4.0/. No attribution
% is required in reports/theses created using this template.
%
%% ----------------------------------------------------------------------
%%    Setting up the class, main packages and basic definitions
%% ----------------------------------------------------------------------

%% Class is based on the default report class and options will be passed
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{chameleon/sty/rep}[17-12-2022 v1.1 EPFL Report Class]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ProcessOptions\relax
\LoadClass[10pt,oneside]{report}

%% Main packages in the document --- Some are imported later in the class file
\RequirePackage{mathtools}  % Mathematical tools to use with amsmath
\RequirePackage{amssymb}    % Extended symbol collection
\RequirePackage{siunitx}    % Comprehensive (SI) units package

\RequirePackage{tabularx}   % Tabulars with adjustable-width columns
\RequirePackage{booktabs}   % Publication quality tables
\RequirePackage{longtable}  % Allow tables to flow over page boundaries
\RequirePackage{multirow}   % Create tabular cells spanning multiple rows

\RequirePackage{graphicx}   % Enhanced support for images
\RequirePackage{float}      % Improved interface for floating objects
\RequirePackage[labelfont=bf,justification=centering,footnotesize]{caption} % Captions
\RequirePackage{subcaption} % Support for sub-captions
\RequirePackage{pdfpages}   % Include PDF documents

\RequirePackage[pdfusetitle,hidelinks]{hyperref} % Extensive support for hypertext
\RequirePackage[noabbrev]{cleveref} % Intelligent cross-referencing
\RequirePackage{xcolor}     % Driver-independent color extensions
\RequirePackage{tikz}       % Create PostScript and PDF graphics
\RequirePackage{xspace}     % Define commands that appear not to eat spaces
\RequirePackage{microtype}  % Refinements towards typographical perfection

\RequirePackage{geometry}   % Customize document dimensions
\RequirePackage{titlesec}   % Select alternative section titles
\RequirePackage{titletoc}   % Alternative headings for toc
\RequirePackage{fancyhdr}   % Control of page headers and footers
\RequirePackage{enumitem}   % Control layout of itemize, enumerate, description
\RequirePackage{etoolbox}   % Toolbox of programming facilities
\RequirePackage{iftex}      % Adds if-else statements to support multiple compilers
\RequirePackage{datetime}   % Change format of \today

%% Establish commands for the subtitle, subject, affiliation, cover image and table of authors
\newcommand*{\subtitle}[1]{\def\@subtitle{#1}}
\newcommand*{\subject}[1]{\def\@subject{#1}}
\newcommand*{\affiliation}[1]{\def\@affiliation{#1}}
\newcommand*{\coverimage}[1]{\def\@coverimage{#1}}
\newcommand*{\covertable}[1]{\def\@covertable{#1}}

%% Scale the margins to be slightly smaller than default (.7)
\geometry{a4paper,hscale=0.75,vscale=0.8}

%% ----------------------------------------------------------------------
%%    Setting up the fonts
%% ----------------------------------------------------------------------
\RequirePackage{fontspec}
\newcommand{\titlestyle}{\fontspec{Lato Light}}
\newcommand{\largetitlestyle}{\fontspec{Lato Light}}

%% ----------------------------------------------------------------------
%%    Formatting the titles and table of contents
%% ----------------------------------------------------------------------

%% Format the chapter titles and spacing
\titleformat{\chapter}[display]
{\flushright}
{\fontsize{96}{96}\selectfont\largetitlestyle\thechapter}
{0pt}
{\Huge\titlestyle}
\titlespacing*{\chapter}{0pt}{0pt}{2\baselineskip}

%% Format the section titles and spacing
\titleformat{\section}
{\Large\titlestyle\bfseries}
{\thesection.}
{5pt}
{}
\titlespacing*{\section}{0pt}{\baselineskip}{0pt}

%% Format the subsections titles and spacing
\titleformat{\subsection}
{\large\titlestyle\bfseries}
{\thesubsection.}
{5pt}
{}
\titlespacing*{\subsection}{0pt}{\baselineskip}{0pt}

%% Format the subsubsections titles and spacing
\titleformat{\subsubsection}
{\titlestyle\bfseries}
{}
{0pt}
{}
\titlespacing*{\subsubsection}{0pt}{\bigskipamount}{0pt}

%% Reduce the vertical white space between chapters in the table of contents
\dottedcontents{chapter}[1.5em]{\vspace{0.5\baselineskip}\bfseries}{1.5em}{0pt}

%% ----------------------------------------------------------------------
%%    Formatting the header and footer
%% ----------------------------------------------------------------------

%% Format the header and footer of 'plain' pages
\fancypagestyle{plain}{%
	\fancyhf{}
	\renewcommand{\headrulewidth}{0pt}
	\fancyfoot[C]{\titlestyle\thepage}}

%% Format the header and footer of 'fancy' pages (based on twoside option)
\if@twoside
	\fancyhf{}
	\fancyhead[LE,RO]{\titlestyle\thepage}
	\fancyhead[RE]{\titlestyle\nouppercase{\leftmark}}
	\fancyhead[LO]{\titlestyle\nouppercase{\rightmark}}
	\RequirePackage{emptypage} % Remove header and footer on empty pages
\else
	\fancyhf{}
	\fancyhead[R]{\titlestyle\thepage}
	\fancyhead[L]{\titlestyle\nouppercase{\rightmark}}
\fi

\pagestyle{fancy} % Set the package defaults and the additional changes as the style

%% ----------------------------------------------------------------------
%%    Setting up the \makecover command for the cover page
%% ----------------------------------------------------------------------

\newcommand*{\makecover}{
	\usetikzlibrary{positioning}
	\thispagestyle{empty}
	
	%% Construct the cover page with Tikz
	\begin{tikzpicture}[overlay,remember picture]
	
		%% Add the cover image
		\node[above=0,inner sep=0] at (current page.south) {%
			\ifdefvoid{\@coverimage}{}{%
				\includegraphics[width=\paperwidth]{\@coverimage}}
		};
	
		% %% Add the affiliation on the left
		% \node[rotate=90,below right=40mm and 3mm] at (current page.west) {%
		% 	\ifdefvoid{\@affiliation}{}{\color{gray}\titlestyle\@affiliation}};
	
		%% Add the logo in the bottom left
		\node[above right=10mm] at (current page.south west) {%
			\includegraphics[width=0.35\linewidth]{chameleon/images/logo-ueve.pdf}};
	
		%% Add the banner with the title, subtitle, subject and author(s)
		\node[below=2cm,minimum width={\paperwidth},inner ysep=25pt,opacity=0.6,text opacity=1] at (current page.north) {%
			\begin{minipage}{0.9\paperwidth}
				%% Format and add the title
				\color{title}\raggedright\largetitlestyle\fontsize{50}{50}\selectfont%
				\@title \\[0.5ex]
				%% Format and add (optional) subtitle and subject
				\color{black}\titlestyle\fontsize{22}{22}\selectfont%
				\ifdefvoid{\@subtitle}{}{\@subtitle \\[1.5ex]}%
				\ifdefvoid{\@subject}{}{\@subject \\[2ex]}
				%% Format and add author or table of authors
				\titlestyle\fontsize{24}{24}\selectfont%
				\ifdefvoid{\@covertable}{\@author}{\@covertable} \\[1.5ex]
                %% Format and addd date 
                \titlestyle\fontsize{18}{18}\selectfont%
                \ifdefvoid{\@date}{2023}{\@date}
			\end{minipage}};

        % Add cover picture
        \node at (current page.center) {
            \includegraphics[]{media/1280px-Great_presidential_puzzle2.jpg}
        };
        
	\end{tikzpicture}

	\newpage
	% avoid numbering of the cover page
	\setcounter{page}{1}
}

%% ----------------------------------------------------------------------
%%    Setting up listings
%% ----------------------------------------------------------------------
\RequirePackage{algorithm}
\RequirePackage{algorithmicx}
\RequirePackage{algpseudocodex}
\renewcommand*{\ALG@name}{Algorithme}

\RequirePackage{booktabs}