\documentclass[11pt]{beamer}
\usetheme{focus}
\include{meta.tex}
\include{colors.tex}

\usepackage{tikz}
\usepackage{amssymb}
\usepackage{minted}
\usepackage[toc,page]{appendix} 
\setminted{
    % bgcolor=mintedbackground,
    fontfamily=tt,
    linenos=true,
    numberblanklines=true,
    numbersep=12pt,
    numbersep=5pt,
    gobble=0,
    frame=leftline,
    framesep=2mm,
    funcnamehighlighting=true,
    tabsize=4,
    obeytabs=false,
    mathescape=false
    samepage=false,
    showspaces=false,
    showtabs =false,
    texcl=false,
    baselinestretch=1.2,
    breaklines=true,
}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\author{ Naïa PÉRINELLE \and Samuel ORTION }
\title{ Jeu de Taquin }
\setbeamercovered{transparent} 
\setbeamertemplate{navigation symbols}{} 
\logo{\includegraphics[width=0.25\textwidth]{./media/univ-evry.fr-logo.noir.png}} 
\institute{ Université d'Évry val d'Essone - Paris-Saclay } 
\date{ 5 Avril 2023 }
\subject{ Intelligence Artificielle } 

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\logo{} % Do not display logo on other slides

\begin{frame}
\tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}{Le taquin}
\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{media/taquin-wood.jpg}
    \caption{Un taquin $4 \times 4$ en bois}
    \label{fig:taquin}
\end{figure}
\end{frame}

\section{Algorithmes}

\begin{frame}{$A^*$ et $IDA^*$}

\begin{description}
    \item [$A^*$] Recherche en largeur, dirigée par une heuristique estimant le chemin qui reste à parcourir;
    \item [$IDA^*$] Recherche en profondeur, limitée en profondeur par l'heuristique de l'état le plus prometteur.
\end{description}

\end{frame}

\section{Heuristiques}

\begin{frame}{Heuristiques pour $n = 3$}

    \[
     h_k (E) = \left( \sum_{i=0}^8 \pi_k (i) \times \varepsilon_E(i) \right) // \rho_k.
    \]

    Où $\varepsilon_E(i)$ est la distance de Manhattan de la tuile $i$ \\
    $\pi_k(i)$ le poids de la tuile $i$ dans l'heuristique $k$ \\
    $\rho_k$ le coefficient de normalisation de l'heuristique $k$.
\end{frame}

\begin{frame}{Heuristiques pour $n > 3$}
    \begin{itemize}
        \item $h_6 = \sum\limits_{i=0}^{n^2 - 2} \varepsilon_E(i)$ \hfill distance de Manhattan brute;
        \item $h_{hic} = \left(\sum\limits_{i=0}^{n^2 - 2} i * \varepsilon_E(i) \right)$;
        \item $h_{mis}$: nombre de tuiles mal placées;
        \item $h_{tic}$: distances de Manhattan, réduite du nombre de tuiles bien placées successivement;
        \item \ldots le max d'une combinaison d'heuristiques admissibles. 
    \end{itemize}
\end{frame}

\section{Implémentation}

\subsection{Structures de données}

\begin{frame}{Frontière}

\begin{itemize}
    \item file de priorité minimale;
    \item \mintinline{python}{queue.PriorityQueue} (python stdlib);
    \item accession par ordre croissant de $g + h$;
\end{itemize}
\end{frame}

\begin{frame}{Ensemble des états explorés}

\begin{figure}
    \centering
    \input{figures/hashtable}
    \caption{Schéma d'une table de hachage}
    \label{fig:hashtable}
\end{figure}

\begin{itemize}
    \item \mintinline{python}{set} (python stdlib);
    \item table de hachage sous le capot;
    \item accession / test d'appartenance en $O(1)$ en principe.
\end{itemize}
\end{frame}

\begin{frame}{Arbre de recherche}
    \inputminted{python}{media/Node.py}
\end{frame}

\section{Étude de performance}

\begin{frame}{État initial}
    \begin{figure}
        \centering
        \includegraphics[width=0.9\textwidth]{media/f(shuffling) = length.png}
        \label{fig:f(shuffling) = length}
    \end{figure}
\end{frame}

\begin{frame}{Comparaison d'heuristique}

\begin{description}
    \item[Optimalité] plus courte longueur de solution
    \item[Temps] temps mis pour trouver les solutions
\end{description}
\end{frame}

\begin{frame}{Nombre de noeuds expansés}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{media/f(heuristique) = expanded.png}
        \label{fig:f(heuristique) = expanded}
    \end{figure}
\end{frame}

\begin{frame}{Longueur de la solution}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{media/f(heuristique) = length.png}
        \label{fig:f(heuristique) = length}
    \end{figure}
\end{frame}

\begin{frame}{Comparaison d'optimalité et temps pour $h_1$}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{media/comp h6 vs h1.png}
        \label{fig:h1 vs h6}
    \end{figure}
\end{frame}

\begin{frame}{Comparaison d'optimalité et temps pour $mis$}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{media/comp h6 vs mis.png}
        \label{fig:mis vs h6}
    \end{figure}
\end{frame}

\begin{frame}{Comparaison d'optimalité et temps pour $hic$}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\textwidth]{media/comp h6 vs hic zoom.png}
        \label{fig:hic vs h6}
    \end{figure}
\end{frame}

\section{Extensions}

\begin{frame}{Application web}

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{media/taquin-web.png}
    \caption{L'affichage d'un taquin $3 \times 3$ à résoudre.}
    \label{fig:taquin-web}
\end{figure}

\url{https://unclesamulus.frama.io/taquin}
    
\end{frame}

% \begin{frame}{\textit{Pattern Database}}
% Pour la résolution des taquins pour $n > 4$,
% \begin{itemize}
%     \item Heuristiques plus précises
% \end{itemize}
% \end{frame}

\begin{frame}[focus]{Merci}
    Merci pour votre attention.
\end{frame}


\begin{frame}{Le dépôt Git du projet}
   \url{https://framagit.org/UncleSamulus/taquin}
\end{frame}

% \begin{frame}{Références}
% \nocite{*} % Display all references regardless of if they were cited
% 	\bibliography{Projet_IA.bib}
% 	\bibliographystyle{plain}
% \end{frame}

\end{document}