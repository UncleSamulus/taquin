"""
Taquin solver and generator

Concatenation of all the modules of the project
"""

import click
import random
import math
import queue

from typing import Callable as function

"""Heuristics"""

HEURISTICS = {
	"h1": {
		"pi": [36, 12, 12, 4, 1, 1, 4, 1, 0],
		"rho": 4
	},
	"h2": {
		"pi": [8, 7, 6, 5, 4, 3, 2, 1, 0],
		"rho": 1
	},
	"h3": {
		"pi": [8, 7, 6, 5, 4, 3, 2, 1, 0],
		"rho": 4
	},
	"h4": {
		"pi": [8, 7, 6, 5, 3, 2, 4, 1, 0],
		"rho": 1
	},
	"h5": {
		"pi": [8, 7, 6, 5, 3, 2, 4, 1, 0],
		"rho": 4
	},
	"h6": {
	},
	"mis": {
	},
	"hic": {
	},
	"lic": {
	},
	"tic": {
	}
}

def misplaced_tiles(state, size):
	"""Number of misplaced tiles"""
	counter = 0
	for i in range(len(state)):
		if state[i] != i:
			counter += 1
	return counter

def heuristic_h6(state, size):
	return sum([
		manhattan_distance(i, state, size) for i in range(len(state) - 1)
	])

def heuristic_hic(state, size):
	"""An attempt to implement a heuristic for n x n grids"""
	def weight(index):
		return index
	return sum([
		weight(i) * manhattan_distance(i, state, size) for i in range(len(state) - 1)
	])

def heuristic_lic(state, size):
	"""Manhattan Distance + Linear Conflicts Heuristic"""
	return sum([
		manhattan_distance(i, state, size) for i in range(len(state) - 1)
	]) + linear_conflicts(state, size)

def heuristic_tic(state, size):
	"""
	Manhattan distance - Well-orneriness
	"""
	well_ordered_counter = 0
	for i in range(len(state)):
		if state[i] == i:
			well_ordered_counter += 1
		else:
			break
	return sum([
		manhattan_distance(i, state, size) for i in range(len(state) - 1)
	]) - well_ordered_counter


def heuristic(name, state, size):
	if name == "hic":
		return heuristic_hic(state, size)
	if name == "mis":
		return misplaced_tiles(state, size)
	if name == "lic":
		return heuristic_lic(state, size)
	if name == "tic":
		return heuristic_tic(state, size)
	if name == "h6":
		return heuristic_h6(state, size)
	pi = HEURISTICS[name]["pi"]
	rho = HEURISTICS[name]["rho"]
	return sum([
		pi[i] * manhattan_distance(i, state, size) for i in range(len(state))
	]) // rho

def manhattan_distance(index, state, size):
	value = state[index]	
	current_x = index % size
	current_y = index // size
	target_x = value % size
	target_y = value // size
	return abs(target_x - current_x) + abs(target_y - current_y)

def inversion(taquin):
	"""Number of inversions
	
	Let a, b be two elements of the taquin.
	There is an inversion if a > b and a is on the left of b.
	"""
	counter = 0
	for i in range(len(taquin)):
		for j in range(i + 1, len(taquin)):
			if taquin[i] > taquin[j]:
				counter += 1
	return counter

def linear_conflicts(taquin, size):
	"""Number of linear conflicts
	
	A linear conflict occurs between tiles ta and tb when ta and tb are both on the same line, 
	and the goal positions of ta is to the right of the goal position of tb,
	"""
	counter = 0
	# For each row and each column
	for i in range(size):
		# For each pair (i, j) of tiles
		for j in range(size - 1):
			for k in range(j+1, size):
				# Row
				# If there is a conflict
				if taquin[i * size + j] > taquin[i * size + k]:
					counter += 1
				# Column
				if taquin[j * size + i] > taquin[k * size + i]:
					counter += 1
	return counter

"""Node"""

class Node:	
	def __init__(self, value):
		self.value = value 
		self.parent = None
		self.priority = 0
		self.depth = 0
		self.heuristic = 0

	def __eq__(self, other):
		return self.value == other.value
	
	def __lt__(self, other):
		return self.priority < other.priority
	

"""Taquin rules implementation"""

MOVES = [
	[0, -1], # UP
	[1, 0], # RIGHT
	[-1, 0], # LEFT
	[0, 1] # DOWN
]

def generate_random_state(size: int, shuffling_iterations: int):
	"""
	Génerer un jeu de taquin solvable, de façon aléatoire
	"""
	state = list(range(size**2)) 
	for _ in range(shuffling_iterations):
		move_direction = random.choice(MOVES)
		new_state = perform_move(state, size, move_direction) # on applique le déplacement
		# si ça a été possible, on stock le déplacement, sinon on ignore
		if new_state is None:
			continue
		else:
			state = new_state
	return state

def perform_move(state: list[int], size: int, direction: list[int]):
	"""
	Compute next state
	"""
	empty_value: int = len(state) - 1 # calcul de la valeur du vide
	empty_position: int = state.index(empty_value) # récuparation de la position du trou
	empty_position_x: int = empty_position % size # calcul du x et du y du trou par quotien et reste de la division
	empty_position_y: int = empty_position // size
	# application du déplacement
	new_empty_position_x: int = empty_position_x + direction[0]
	new_empty_position_y: int = empty_position_y + direction[1]
	# test de la validité du déplacement, si non valide, on ne renvoie rien
	if new_empty_position_x < 0 or new_empty_position_x >= size or new_empty_position_y < 0 or new_empty_position_y >= size:
		return None
	# on calcul par application du quotien et du reste la nouvelle position dans la liste
	new_empty_position: int = new_empty_position_y * size + new_empty_position_x
	# on fait une copie de l'état et on fait le déplacement
	new_state = state.copy()
	new_state[empty_position] = new_state[new_empty_position]
	new_state[new_empty_position] = empty_value
	return new_state

def reconstruct_move(previous_state: list[int], current_state: list[int], size: int):
	"""
	Get the move played from previous_state to current_state
	"""
	empty_value: int = len(previous_state) - 1
	empty_position: int = previous_state.index(empty_value)
	empty_position_x: int = empty_position % size
	empty_position_y: int = empty_position // size
	new_empty_position: int = current_state.index(empty_value)
	new_empty_position_x: int = new_empty_position % size
	new_empty_position_y: int = new_empty_position // size
	delta_x: int = new_empty_position_x - empty_position_x
	delta_y: int = new_empty_position_y - empty_position_y
	return [delta_x, delta_y]

def action(move_direction: list[int]):
	"""
	Convert coordinate moves to action description
	"""
	if move_direction == [0, -1]:
		return "UP"
	elif move_direction == [1, 0]:
		return "RIGHT"
	elif move_direction == [-1, 0]:
		return "LEFT"
	elif move_direction == [0, 1]:
		return "DOWN"
	else:
		print(move_direction)
		raise Exception("Invalid move direction")

def is_goal(state: list[int]):
	"""
	Check if given state is goal
	"""
	for i in range(len(state)):
		if state[i] != i:
			return False
	return True

def get_possible_moves(state: list[int], size: int):
	"""
	Compute all possible next states
	"""
	# pour chaque mouvements, vérife s'il est acceptable et renvoie la liste des nouveau états possibles
	for move_direction in MOVES:
		new_state = perform_move(state, size, move_direction)
		if new_state is not None:
			yield new_state

def print_state(state: list[int], size: int):
	for i in range(size):
		for j in range(size):
			print("{0: <2}".format(state[i*size+j]), end=' ')
		print()

def validity_check(taquin):
	"""Perform a parity check on the taquin"""
	is_even = lambda x: x % 2 == 2
	inversions = inversion(taquin)
	size = math.sqrt(len(taquin)) 
	if not is_even(size):
		return is_even(inversions)
	else: 
		empty_position = taquin.index(size**2 - 1)
		if not is_even(empty_position):
			return not is_even(inversions)
		else:
			return is_even(inversions)

"""
Search functions implementation

- Classical A*
- Iterative Deepening A*
- more ?
"""

def a_star_search(initial_node: Node, expand_function, goal_test_function):
	"""
	A* search algorithm

	:param initial_node: the initial node
	:param expand_node_function: the function to expand a node
	:param goal_test_function: the function to test if a node is a goal
	:return: the goal node if found, None otherwise
	"""
	frontier: queue.PriorityQueue = queue.PriorityQueue()
	frontier.put(initial_node)
	explored: set[list] = set() # Set uses hashtable under the hood.
	while not frontier.empty():
		node: Node = frontier.get()
		explored.add(tuple(node.value))	
		if goal_test_function(node):
			return node
		successors = expand_function(node)
		for successor in successors:
			if tuple(successor.value) not in explored:
				frontier.put(successor)
	return None

def iterative_deepening_a_star(initial_node: Node, expand_function: function, goal_test_function: function, depth_limit: int = 500):
	"""
	Perform A* search using iterative deepening.

	ref. https://en.wikipedia.org/wiki/Iterative_deepening_A*

	(Not quite sure if this is the correct implementation)
	
	:param initial_node: the initial node
	:param expand_node_function: the function to expand a node
	:param goal_test_function: the function to test if a node is a goal
	:return: the goal node if found, None otherwise
	"""

	def search(node: Node, path: list[Node], threshold_cost: int):
		if node is None:
			return False, None, float("inf")
		f = node.priority # node.priority = node.depth + node.heuristic
		if f > threshold_cost:
			return False, node, f
		if goal_test_function(node):
			return True, node, f
		minimal_estimate = float("inf")
		successors = expand_function(node)
		for successor in successors:
			if successor not in path:
				found, node, estimate = search(successor, path + [node], threshold_cost)
				if found:
					return True, node, estimate
				if estimate < minimal_estimate:
					minimal_estimate = estimate
		return False, node, minimal_estimate

	bound = initial_node.heuristic
	node = initial_node
	path = []
	while True:
		found, node, estimate = search(node, path, bound)
		if found:
			return node
		if estimate == float("inf"):
			return None
		bound = estimate
		
"""Solve"""

def solve(initial_taquin, size, heuristic_name, algorithmic_name):
	def expand_function(node):
		successors = []
		for move in get_possible_moves(node.value, size):
			successor = Node(move)
			successor.parent = node
			successor.depth = node.depth + 1
			successor.heuristic = heuristic(heuristic_name, move, size)
			successor.priority = successor.depth + successor.heuristic
			successors.append(successor)
		return successors

	def goal_test_function(node):
		return is_goal(node.value)
	
	initial_node = Node(initial_taquin)
	if algorithmic_name == "astar":
		leaf_node = a_star_search(initial_node, expand_function, goal_test_function)
	elif algorithmic_name == "idastar":
		leaf_node = iterative_deepening_a_star(initial_node, expand_function, goal_test_function)
	if leaf_node is None:
		return None
	else:
		return path_from_root(leaf_node)

def path_from_root(leaf_node):
	"""
	Reconstruct the path from root node to solution"""
	path = []
	node = leaf_node
	while node is not None:
		path.append(node.value)
		node = node.parent
	return path[::-1]

"""CLI"""

@click.command()
@click.option('-v', '--verbose', 'verbose', is_flag=True, default=False)
@click.option('-n', '--size', 'size', default=3)
@click.option('-r', '--random', 'random_iterations', default=1000)
@click.option('-h', '--heuristic', 'heuristic_name', default='h1', type=click.Choice(HEURISTICS))
@click.option('-a', '--algorithm', 'algorithm_name', default="astar", type=click.Choice(['astar', 'idastar']))
@click.option('-m', '--move-only', 'move_only', is_flag=True, default=False)
@click.option('-l', '--list-heuristics', 'list_heuristics', is_flag=True, default=False)
@click.option('-t', '--taquin', 'taquin_str', default=None, help="Taquin to solve, list of numbers row by row, comma separated.")
@click.option('-s', '--seed', 'seed', default=None, help="Seed for random taquin generation")
def cli(verbose, size, random_iterations, heuristic_name, move_only, list_heuristics, algorithm_name, taquin_str, seed):
	if list_heuristics:
		print("Liste des heuristiques:")
		for name in HEURISTICS:
			print(name)
		return
	if taquin_str is not None:
		initial_taquin = parse_taquin(taquin_str)
		if not validity_check(initial_taquin):
			print("Unsolvable taquin")
			return
	else: 
		if seed is not None:
			random.seed(seed)
		initial_taquin = generate_random_state(size, random_iterations)
	if verbose:
		print_state(initial_taquin, size)
		print()
	# Check if parameters are valid
	# If heuristic is not fit to the size of the taquin, exit
	if not heuristic_name in ["hic", "mis", "lic", "tic", "h6"] and size**2 > len(HEURISTICS[heuristic_name]["pi"]):
		print("Invalid heuristic for this size of taquin")
		return
	solution = solve(initial_taquin, size, heuristic_name, algorithm_name)
	if solution is None and verbose:
		print("Pas de solution")
	else:
		if verbose:
			print("Solution en {} étapes:".format(len(solution)-1))
		for state in solution[1:]:
			if verbose or move_only:
				print("Action: {}".format(action(reconstruct_move(solution[solution.index(state)-1], state, size))))
			if not move_only:
				print_state(state, size)
				print()

def parse_taquin(string):
	"""
	Parse a string to a taquin
	"""
	return [int(x) for x in string.split(",")]

def main():
	cli()

if __name__ == "__main__":
	main()