
class Node:	
	def __init__(self, value):
		self.value = value 
		self.parent = None 
		self.priority = 0
		self.depth = 0
		self.heuristic = 0

	def __eq__(self, other):
		return self.value == other.value
	
	def __lt__(self, other):
		return self.priority < other.priority
	