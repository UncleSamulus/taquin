import unittest
from .. import heuristics

class TestHeuristicMethod(unittest.TestCase):
	def test_distance(self):
		state = [
			1, 3, 8, 
			5, 7, 6,
			4, 2, 0
		]
		self.assertEqual(heuristics.manhattan_distance(1-1, state, 3), 1) # $\varepslion_E(1) = 1$
		#...
	def test_linear_conflicts(self):
		state = [
			0, 1, 2,
			3, 5, 4,
			6, 7, 8
		]
		self.assertEqual(heuristics.linear_conflicts(state, 3), 1)

def main():
	unittest.main()

if __name__ == "__main__":
	main()