import unittest
from .. import solve

class TestSolveMethod(unittest.TestCase):
	def test_solve(self):
		state = [
			1, 3, 8, 
			5, 7, 6,
			4, 2, 0
		]
		print(solve.solve(state, 3, "pi1"))

def main():
	unittest.main()
	