# lance la résolution du taquin et construit la solution

from . import search
from . import taquin
from .Node import Node
from . import heuristics

def solve(initial_taquin, size, heuristic_name, algorithmic_name):
	def expand_function(node):
		successors = []
		for move in taquin.get_possible_moves(node.value, size):
			successor = Node(move)
			successor.parent = node
			successor.depth = node.depth + 1
			successor.heuristic = heuristics.heuristic(heuristic_name, move, size)
			successor.priority = successor.depth + successor.heuristic
			successors.append(successor)
		return successors

	def goal_test_function(node):
		return taquin.is_goal(node.value)
	
	initial_node = Node(initial_taquin)
	if algorithmic_name == "astar":
		leaf_node = search.a_star_search(initial_node, expand_function, goal_test_function)
	elif algorithmic_name == "idastar":
		leaf_node = search.iterative_deepening_a_star(initial_node, expand_function, goal_test_function)
	if leaf_node is None:
		return None
	else:
		return path_from_root(leaf_node)

# renvoie le chemin de l'état initial à l'état final
def path_from_root(leaf_node):
	path = []
	node = leaf_node
	while node is not None:
		path.append(node.value)
		node = node.parent
	return path[::-1]