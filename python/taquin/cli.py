#!/usr/bin/env python3
"""
Le jeu du Taquin - CLI

Trouver une solution au Taquin

Usage:
	python3 -m taquin.cli --help
	python3 -m taquin.cli -n 3 -a astar -h h1
"""

from . import taquin
from . import solve
from . import heuristics

import click
import random

@click.command()
@click.option('-v', '--verbose', 'verbose', is_flag=True, default=False)
@click.option('-n', '--size', 'size', default=3)
@click.option('-r', '--random', 'random_iterations', default=1000)
@click.option('-h', '--heuristic', 'heuristic_name', default='h1', type=click.Choice(heuristics.HEURISTICS))
@click.option('-a', '--algorithm', 'algorithm_name', default="astar", type=click.Choice(['astar', 'idastar']))
@click.option('-m', '--move-only', 'move_only', is_flag=True, default=False)
@click.option('-l', '--list-heuristics', 'list_heuristics', is_flag=True, default=False)
@click.option('-t', '--taquin', 'taquin_str', default=None, help="Taquin to solve, list of numbers row by row, comma separated.")
@click.option('-s', '--seed', 'seed', default=None, help="Seed for random taquin generation")
def cli(verbose, size, random_iterations, heuristic_name, move_only, list_heuristics, algorithm_name, taquin_str, seed):
	if list_heuristics:
		print("Liste des heuristiques:")
		for name in heuristics.HEURISTICS:
			print(name)
		return
	if taquin_str is not None:
		initial_taquin = parse_taquin(taquin_str)
		if not taquin.validity_check(initial_taquin):
			print("Unsolvable taquin")
			return
	else: 
		if seed is not None:
			random.seed(seed)
		initial_taquin = taquin.generate_random_state(size, random_iterations)
	if verbose:
		taquin.print_state(initial_taquin, size)
		print()
	# Check if parameters are valid
	# If heuristic is not fit to the size of the taquin, exit
	if not heuristic_name in ["hic", "mis", "lic", "tic", "h6"] and size**2 > len(heuristics.HEURISTICS[heuristic_name]["pi"]):
		print("Invalid heuristic for this size of taquin")
		return
	solution = solve.solve(initial_taquin, size, heuristic_name, algorithm_name)
	if solution is None and verbose:
		print("Pas de solution")
	else:
		if verbose:
			print("Solution en {} étapes:".format(len(solution)-1))
		for state in solution[1:]:
			if verbose or move_only:
				print("Action: {}".format(taquin.action(taquin.reconstruct_move(solution[solution.index(state)-1], state, size))))
			if not move_only:
				taquin.print_state(state, size)
				print()

def parse_taquin(string):
	"""
	Parse a string to a taquin
	"""
	return [int(x) for x in string.split(",")]

def main():
	cli()

if __name__ == "__main__":
	main()