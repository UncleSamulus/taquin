"""
Search functions implementation

- Classical A*
- Iterative Deepening A*
- more ?
"""

from typing import Callable as function

import queue

from .Node import Node

def a_star_search(initial_node: Node, expand_function, goal_test_function):
	"""
	A* search algorithm

	:param initial_node: the initial node
	:param expand_node_function: the function to expand a node
	:param goal_test_function: the function to test if a node is a goal
	:return: the goal node if found, None otherwise
	"""
	frontier: queue.PriorityQueue = queue.PriorityQueue()
	frontier.put(initial_node)
	explored: set[list] = set() # Set uses hashtable under the hood.
	while not frontier.empty():
		node: Node = frontier.get()
		explored.add(tuple(node.value))	
		if goal_test_function(node):
			return node
		successors = expand_function(node)
		for successor in successors:
			if tuple(successor.value) not in explored:
				frontier.put(successor)
	return None

def iterative_deepening_a_star(initial_node: Node, expand_function: function, goal_test_function: function, depth_limit: int = 500):
	"""
	Perform A* search using iterative deepening.

	ref. https://en.wikipedia.org/wiki/Iterative_deepening_A*

	(Not quite sure if this is the correct implementation)
	
	:param initial_node: the initial node
	:param expand_node_function: the function to expand a node
	:param goal_test_function: the function to test if a node is a goal
	:return: the goal node if found, None otherwise
	"""

	def search(node: Node, path: list[Node], threshold_cost: int):
		if node is None:
			return False, None, float("inf")
		f = node.priority # node.priority = node.depth + node.heuristic
		if f > threshold_cost:
			return False, node, f
		if goal_test_function(node):
			return True, node, f
		minimal_estimate = float("inf")
		successors = expand_function(node)
		for successor in successors:
			if successor not in path:
				found, node, estimate = search(successor, path + [node], threshold_cost)
				if found:
					return True, node, estimate
				if estimate < minimal_estimate:
					minimal_estimate = estimate
		return False, node, minimal_estimate

	bound = initial_node.heuristic
	node = initial_node
	path = []
	while True:
		found, node, estimate = search(node, path, bound)
		if found:
			return node
		if estimate == float("inf"):
			return None
		bound = estimate
